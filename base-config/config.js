module.exports = {
  platform: "gitlab",
  endpoint: "https://gitlab.com/api/v4/",
  token: process.env.RENOVATE_TOKEN,
  timezone: "Europe/Amsterdam",
  requireConfig: true,
  onboarding: true,
  onboardingConfig: {
    extends: ["config:base"],
    prConcurrentLimit: 5,

  },
  "hostRules": [
    {
      "hostType": "docker",
      "hostName": "cr.geekfarmer.dev",
      "username": process.env.NEXUS_USERNAME,
      "password": process.env.NEXUS_PASSWORD,
    },
    {
      "matchHost": process.env.NEXUS_PIP,
      "hostType": "pypi",
      "username": process.env.NEXUS_USERNAME,
      "password": process.env.NEXUS_PASSWORD
    },
  ],
};

