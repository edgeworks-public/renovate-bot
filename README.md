# Renovate Integration with Sonatype Nexus & Artifactory
[Badge](https://gitlab.com/renjithvr11/renovate-jfrog/badges/main/pipeline.svg)

Here you would samples to integrate Self hosted Renovate with Sonatype Nexus

## CI/CD Variables
- RENOVATE_TOKEN

## Reference
- https://github.com/renovatebot/renovate/blob/main/docs/usage/getting-started/private-packages.md
- https://docs.renovatebot.com/modules/platform/gitlab/
- https://docs.renovatebot.com/config-presets/#gitlab
- https://github.com/renovatebot/renovate/blob/main/docs/usage/docker.md